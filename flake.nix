{
  description = "Let some website figure out how you really feel";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs";
    poetry2nix.url = "github:nix-community/poetry2nix";
  };

  outputs = { self, nixpkgs, flake-utils, poetry2nix }:
    {
      overlay = nixpkgs.lib.composeManyExtensions [
        poetry2nix.overlay
        (final: prev: {
          extrospection-lib = prev.poetry2nix.mkPoetryApplication {
            projectDir = ./.;
          };
        })
      ];
    } // (flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ self.overlay ];
        };
      in
      {
        packages = {
          extrospection-lib = pkgs.extrospection-lib;
        };

        defaultPackage = pkgs.extrospection-lib;

        devShell = pkgs.mkShell {
          buildInputs = with pkgs; [
            poetry

            (python39.withPackages(pythonPackages: with pythonPackages; [
              python-lsp-server
            ]))
          ];
        };
      }));
}
