# standard
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from redis.client import Redis
    from typing import Any, Optional

# external
import redis

DEFAULT_MAX_LENGTH = 100


class Connection:
    def __init__(self):
        self.redis_connection: "Optional[Redis]" = None
        self._redis_pool: "Optional[Redis]" = None

    def with_connection_url(
        self,
        url: "str",
        *args,
        **kwargs,
    ) -> "Connection":
        self._redis_pool = redis.from_url(url, *args, **kwargs)

        return self

    def __enter__(self):
        assert self._redis_pool is not None

        if self.redis_connection is None:
            self.redis_connection = self._redis_pool.client().__enter__()

        return Broker(self)

    def __exit__(self, exc_type, exc_value, traceback):
        if self.redis_connection is not None:
            self.redis_connection.__exit__(exc_type, exc_value, traceback)

            self.redis_connection = None


class Broker:
    def __init__(self, connection: "Connection"):
        assert connection.redis_connection is not None

        self.redis_connection: "Redis" = connection.redis_connection
        self._max_length: "int" = DEFAULT_MAX_LENGTH

    def with_max_length(self, max_length: "int") -> "Broker":
        self._max_length = max_length

        return self

    def publish(self, key: "str", message: "dict") -> "Any":
        return self.redis_connection.xadd(
            key,
            message,
            maxlen=self._max_length,
        )
